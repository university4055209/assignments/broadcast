#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include <unistd.h>


//============================== BROADCAST ====================================
// Assumptions: 
// The program assumes that no packets are received unordered. This means that 
// if a node received a packet with seq_n > last_seen_seq_n + 1 said packet 
// would be discarded. To avoid this behaviour, a buffer could be issued for 
// each node.
//
// The code: 
// The implementation creates a socket for each node, then checks if the node
// is the leader - id == 0 - and, if that is the case, sends the first message.
// Since there is only one sender which send one packet only, we could avoid 
// using sequence numbers alltogether; for example a bool 'received' variable 
// could be used to check whether the packet already reached a node. However, 
// since the 'traffic-generators' exercices needs a more complex definition of 
// a packet type, I decided to employ it here too. A packet, as the struct 
// directly under this comment specifies, is made of its paylod, its sender_id 
// and its sender_sequence_number. When a packet is received, the node then 
// checks whether its sender is in its known hosts and what packet it received
// that STARTED from said host. 
// ============================================================================


#define PORT 9999

typedef struct packet{
    uint16_t sender_id;
    uint16_t sequence_number;
    uint32_t payload;
}packet;

typedef struct knownHostsNode {
    packet * contents;
    struct knownHostsNode * next;
}knownHostsNode;

typedef knownHostsNode* knownHostsList;

void log(int id, char * format, ...) {
    va_list args;
    va_start(args, format);
    printf("%d: ", id);
    printf(format, args);
    va_end(args);
}

//Returns true if message was seen
bool checkMessage(packet * msg, knownHostsList list, int id){
    printf("DEBUG:\n\tsender:%u\n\tsequence_n:%u\n\tpayload:%u\n",
           msg->sender_id, msg->sequence_number, msg->payload);
    log(id, "scanning msg from node %h with sequence_number\
        %h\n", msg->sender_id, msg->sequence_number);
    if(list==NULL){
        knownHostsList list = (knownHostsList)calloc(1, sizeof(knownHostsNode));
        list->contents = (packet *)calloc(1, sizeof(packet));
        log(id, "New message with seq_n %d from new host %d\n",
            msg->sequence_number, msg->sender_id);
        list->contents->sender_id = msg->sender_id;
        list->contents->sequence_number = msg->sequence_number;
        return false;
    }
    do{
        if(list->contents->sender_id == msg->sender_id){
            if(list->contents->sequence_number >= msg->sequence_number){
                log(id, "Message already received, skipping...\n");
                return true;
            }
            else if(list->contents->sequence_number == msg->sequence_number-1){
                //Message was not seen
                log(id, "New message from %d! Updating seq_n\
                    (%d)", msg->sender_id, msg->sequence_number);
                list->contents->sequence_number++;
                return false;
            }
            else{
                //Message was not seen but some acks were lost
                list->contents->sequence_number = msg->sequence_number; 
                log(id, "message not in order\n"); 
                return false;
            } 
        }
        if(list->next == NULL){
            log(id, "New message with seq_n %d from new host %d\n",
                msg->sequence_number, msg->sender_id);
            list->next = (knownHostsList)calloc(1, sizeof(knownHostsNode));
            list->next->contents = (packet *)calloc(1, sizeof(packet));
            list->next->contents->sender_id = msg->sender_id;
            list->next->contents->sequence_number = msg->sequence_number;
            return false;
        }
    }while(list!=NULL); //This should never be the case
    log(id, "WARNING: SOME ERROR OCCURED WHILE SCANNING KNOWN_HOSTS\n");
    return false;
}

int main(int argc, char *argv[])
{
    setvbuf(stdout, NULL, _IONBF, 0);

    if (argc < 3) {
        perror("No neighbor for this node");
        return 1;
    }

    int id = atoi(argv[1]);

    int * neighbors = calloc(sizeof(int), argc - 2);
    for (int i = 0; i < argc - 2; i ++) {
        neighbors[i] = atoi(argv[i + 2]);
    }

    log(id, "begin");

    // Implementation
    knownHostsList list = NULL;

    int sock; //node socket
    int yes = 1; //Sets the broadcast option to true
    struct sockaddr_in broadcast_addr;
    struct sockaddr_in server_addr;
    int addr_len;
    int count; //Used to count received bytes
    int ret; //Used for error checking
    fd_set readfd;
    char buffer[1024]; 
    packet * sendbuf = NULL;
    log(id, "Inizializing...\n");

    sock = socket(AF_INET, SOCK_DGRAM, 0); //Creates an UDP socket
    if (sock < 0)
    {
        perror("sock error");
        return -1;
    }

    //Sets support for broadcast communication
    ret = setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (char *)&yes, sizeof(yes));
    if (ret == -1)
    {
        perror("setsockopt error");
        return 0;
    }

    addr_len = sizeof(struct sockaddr_in);
    memset((void *)&broadcast_addr, 0, addr_len);
    broadcast_addr.sin_family = AF_INET;
    broadcast_addr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
    broadcast_addr.sin_port = htons(PORT);

    if(id == 0){
        //Leader
        sendbuf = (packet *)calloc(1, sizeof(packet));
        sendbuf->payload = rand()%100;
        sendbuf->sender_id = id;
        sendbuf->sequence_number = 1; //First sequence_number
        checkMessage(sendbuf, list, id); //Call to check to insert my own message
        //in the known host list (loopback)
        ret = sendto(sock, sendbuf, sizeof(packet), 0, (struct sockaddr *)&broadcast_addr, addr_len);
    }
    printf("before the while!\n");
    while(1){
        printf("in the while!\n");
        memset(buffer, 0, 1024);
        FD_ZERO(&readfd);
        FD_SET(sock, &readfd);
        
        log(id, "Waiting for message");
        ret = select(sock + 1, &readfd, NULL, NULL, NULL);
        if(ret > 0){
            if(FD_ISSET(sock, &readfd)){
                count = recvfrom(sock, buffer, 1024, 0, (struct sockaddr *)&server_addr, (socklen_t*) &addr_len);
                log(id, "Received new message\n from %s:%d", inet_ntoa(server_addr.sin_addr), htons(server_addr.sin_port));
                //checks message contents 
                packet *msg = (packet* ) buffer;
                if(msg == NULL){
                    log(id, "Error: message received in wrong format");
                    continue;
                }
                if(!checkMessage(msg, list, id)){
                    log(id, "Message contents: %d", msg->payload);
                    //There's no need to create a new buffer, since i need to 
                    //resend the message 'as-is'.
                    log(id, "Prepare broadcast message\n");            
                    ret = sendto(sock, msg, sizeof(packet), 0, (struct sockaddr *)&broadcast_addr, addr_len);
                }
            }
        }
    }
    free(neighbors);

}
