# On the use of `select()`: 

As we can read from its [manpage](https://man7.org/linux/man-pages/man2/select.2.html), the `select()` function can be used to swiftly handle multiple file descriptors. The fd's are manipulated through `fd_set`'s, an abstraction that represents a set of file descriptors. The maximum set size if 1024; for a more complex system `poll()` can be used instead.   
Here is a rundown on its related macros:  

| Macro                                 | Use                                    |
| ------------------------------------- | -------------------------------------- |
| `void FD_ZERO(fd_set *set);`          | Empties `set` from any file descriptor |
| `void FD_SET(int fd, fd_set *set);`   | Adds `fd` to `set`                     |
| `int  FD_ISSET(int fd, fd_set *set);` | Checks if `fd` is in `set`             |
| `void FD_CLR(int fd, fd_set *set);`   | Removes `fd` from `set`                |

The function call is **`int select(int nfds, fd_set *_Nullable restrict readfds, fd_set *_Nullable restrict writefds, fd_set *_Nullable restrict exceptfds, struct timeval *_Nullable restrict timeout);`**.  
+ `nfds` should be the highest fd number plus one: `select` will check all fd's with a value lower than `nfds`;
+ `readfds` should be a `fd_set` containing all the fd's to check for reading;
+ `writefds` should be a `fd_set` containing all the fd's to check for writing (assuming that writes are not too long to be split);
+  The fd's in `execptfds` set are watched for "exceptional conditions";
+  `timeout` specifies how long should the `select()` wait until a *ready* event occurs. If `NULL`, the function waits indefinitely until blocked by a signal/the OS.

In this udp example the `select()` is used to wait on the opened `sock`. When the function returns successfully, the socket is ready to be parsed for the received message.